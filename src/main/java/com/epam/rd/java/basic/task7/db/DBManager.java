package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

import javax.imageio.IIOException;

import static java.sql.DriverManager.getConnection;


public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() {
        Properties properties = new Properties();
        try {
            InputStream in = new FileInputStream("app.properties");
            properties.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Connection connection = null;

        String connectionURL = properties.getProperty("connection.url");
        String username = properties.getProperty("username");
        String password = properties.getProperty("password");

        try {
            connection = DriverManager.getConnection(connectionURL, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();

        try (Connection connection = getConnection()) {
            Statement findAllStatement = connection.createStatement();

            ResultSet resultSet = findAllStatement.executeQuery(
                    "SELECT * FROM users");

            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getInt("id"));
                user.setLogin(resultSet.getString("login"));

                users.add(user);
            }
        } catch (SQLException e) {

        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        if (user == null) return false;

        try (Connection connection = getConnection()) {
            PreparedStatement insertStatement = connection.prepareStatement(
                    "INSERT INTO users(login) VALUES(?)", Statement.RETURN_GENERATED_KEYS);

            insertStatement.setString(1, user.getLogin());

            int affectedRows = insertStatement.executeUpdate();
            if (affectedRows == 0) return false;

            ResultSet resultSet = insertStatement.getGeneratedKeys();
            if (resultSet.next()) user.setId(resultSet.getInt(1));

        } catch (SQLException e) {
            throw new DBException("Cannot insert user" + user.getLogin(), e.getCause());
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (Connection connection = getConnection()) {
            PreparedStatement deleteUserStatement = connection.prepareStatement(
                    "DELETE FROM users WHERE login=?");

            for (User user : users) {
                deleteUserStatement.setString(1, user.getLogin());
                deleteUserStatement.executeUpdate();
            }

        } catch (SQLException e) {
            throw new DBException("Cannot delete users", e.getCause());
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        User user = new User();
        try (Connection connection = getConnection()) {
            PreparedStatement getUserStatement = connection.prepareStatement(
                    "SELECT * FROM users WHERE login=?");

            getUserStatement.setString(1, login);

            ResultSet resultSet = getUserStatement.executeQuery();

            resultSet.next();

            user.setId(resultSet.getInt("id"));
            user.setLogin(login);

        } catch (SQLException e) {
            throw new DBException("Cannot get user " + login, e.getCause());
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = new Team();
        try (Connection connection = getConnection()) {
            PreparedStatement getTeamStatement = connection.prepareStatement(
                    "SELECT * FROM teams WHERE name=?");

            getTeamStatement.setString(1, name);

            ResultSet resultSet = getTeamStatement.executeQuery();

            resultSet.next();

            team.setId(resultSet.getInt("id"));
            team.setName(name);

        } catch (SQLException e) {
            throw new DBException("Cannot get team " + name, e.getCause());
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection connection = getConnection()) {
            Statement findAllTeamsStatement = connection.createStatement();
            ResultSet resultSet = findAllTeamsStatement.executeQuery(
                    "SELECT * FROM teams");

            while (resultSet.next()) {
                Team team = new Team();
                team.setId(resultSet.getInt("id"));
                team.setName(resultSet.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("Cannot find all teams", e.getCause());
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        try (Connection connection = getConnection()) {
            PreparedStatement insertStatement = connection.prepareStatement(
                    "INSERT INTO teams(name) VALUES(?)", Statement.RETURN_GENERATED_KEYS);

            insertStatement.setString(1, team.getName());

            int affectedRows = insertStatement.executeUpdate();
            if (affectedRows == 0) return false;
            ResultSet resultSet = insertStatement.getGeneratedKeys();
            if(resultSet.next()) team.setId(resultSet.getInt(1));

        } catch (SQLException e) {

        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        try {
            connection = getConnection();
            connection.setAutoCommit(false);

            for (Team team : teams) {
                PreparedStatement insertStatement = connection.prepareStatement(
                        "INSERT INTO users_teams(user_id, team_id) VALUES(?, ?)");

                insertStatement.setInt(1, user.getId());
                insertStatement.setInt(2, team.getId());

                insertStatement.executeUpdate();
            }

            connection.commit();
            connection.close();
            return true;

        } catch (SQLException e) {
            try {
                connection.rollback();

            } catch (SQLException ex) {

            }
            throw new DBException("Cannot set Teams for User", e.getCause());
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        List<Integer> teamsId = new ArrayList<>();

        try (Connection connection = getConnection()) {
            PreparedStatement findAllTeams = connection.prepareStatement(
                    "SELECT * FROM users_teams WHERE user_id=?");
            findAllTeams.setInt(1, user.getId());

            ResultSet resultSet = findAllTeams.executeQuery();

            while (resultSet.next()) {
                teamsId.add(resultSet.getInt("team_id"));
            }
        } catch (SQLException e) {
            throw new DBException("Cannot get user teams", e.getCause());
        }

        for (Integer id : teamsId) {
            try (Connection connection = getConnection()) {
                PreparedStatement findAllTeams = connection.prepareStatement(
                        "SELECT * FROM teams WHERE id=?");
                findAllTeams.setInt(1, id);

                ResultSet resultSet = findAllTeams.executeQuery();
                while (resultSet.next()) {
                    Team team = new Team();
                    team.setId(id);
                    team.setName(resultSet.getString("name"));
                    teams.add(team);
                }
            } catch (SQLException e) {
                throw new DBException("Cannot get user teams", e.getCause());
            }
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        try (Connection connection = getConnection()) {
            PreparedStatement deleteTeamStatement = connection.prepareStatement(
                    "DELETE FROM teams WHERE id =? AND name=?");

            deleteTeamStatement.setInt(1, team.getId());
            deleteTeamStatement.setString(2, team.getName());

            deleteTeamStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Cannot delete team", e.getCause());
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        try (Connection connection = getConnection()) {
            PreparedStatement updateTeamStatement = connection.prepareStatement(
                    "UPDATE teams SET name =? WHERE id=?");

            updateTeamStatement.setString(1, team.getName());
            updateTeamStatement.setInt(2, team.getId());

            updateTeamStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Cannot update team", e.getCause());
        }
        return true;
    }

}
