package com.epam.rd.java.basic.task7.db;

public class DBException extends Exception {

	public DBException(String message, Throwable cause) {
		System.err.println("Message is: " + message + ". Cause is: " + cause + ".");
	}

}
